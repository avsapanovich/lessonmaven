package org.itstep.qa.lesson;

import org.kohsuke.randname.RandomNameGenerator;

public class SimpleRunner {
    public static void main(String[] args){
        String names[] = new String[10];
        RandomNameGenerator rnd = new RandomNameGenerator(0);
        for(int i=0; i<names.length; i++){
            names[i] = rnd.next();
        }

        for (String name : names){
            System.out.println(name);
        }
    }
}
